# 
# Helper functions for ApolloSM reads.
# 

from ApolloSM import ApolloSM

def read_register(register_name: str, sm_instance: ApolloSM) -> int:
    """
    Perform a read for the given register, and return the result as a string.
    Returns None if the register cannot be read.
    """
    
    result = None

    try:
        result = sm_instance.ReadRegister(register_name)
        result = hex(result)
    
     # Check that we were able to read register and indeed got a valid FW hash
    except Exception:
        raise RuntimeError(f"Could not read register: {register_name}")

    if result is None:
        raise RuntimeError(f'Could not read register: {register_name}')

    return result


def write_to_register(register_name: str, data: int, sm_instance: ApolloSM):
    """
    Writes the given data to the provided register using ApolloSM write command.

    Checks if the write has been successful by reading back the same register,
    and raises a RuntimeError if the value read is not the same as the value written.
    """
    sm_instance.WriteRegister(register_name, data)

    try:
        reg_value = read_register(register_name, sm_instance)

    # We failed to read the data back, raise an error specifying that the write is unsuccessful
    except Exception:
        raise RuntimeError(f"Unsuccessful write to: {register_name}")

    if data != int(reg_value, base = 16):
        raise RuntimeError(f"Unsuccessful write to: {register_name}")
