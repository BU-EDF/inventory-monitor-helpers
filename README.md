# Inventory Monitoring SW Helpers

This repository contains helper libraries/function definitions for several inventory monitoring software. 

Meant to be used as a submodule for the main inventory monitoring software modules, **do not** try to run the Python3 scripts on this repository on their own.

## Adding This Repo As A Submodule

To add this repository as a submodule to another monitoring software, you can do:

```bash
git submodule add https://gitlab.com/BU-EDF/inventory-monitor-helpers.git helpers
```

This will save this repository as a submodule under `helpers` directory, within the main project.