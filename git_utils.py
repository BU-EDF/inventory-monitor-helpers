# 
# Utilities for GitHub and GitLab API.
# 

import github
import gitlab

from typing import Dict
from enum import IntEnum


class FWStatus(IntEnum):
    """
    Enum representing the FW commit compared to the latest tag.
    """
    DEVICE_NOT_PRESENT = 0     # No device to read FW from
    DB_ERROR = 1               # Error connecting to the database / GitHub API
    FW_BEHIND = 2              # FW is behind the latest tag
    FW_IDENTICAL = 4           # FW is identical to the latest tag
    FW_AHEAD = 7               # FW is ahead of the latest tag


def compare_git_version_with_latest_tag(
    project_identifier, 
    reghelper,
    register, 
    logger,
    api_token,
    commit,
    repo_host,
    ):
    """
    Wrapper function to compare the given commit to the latest tag obtained from GitHub or GitLab.
    """
    # Check the API token
    if api_token == "":
       logger.warning(f"No API token found for: {repo_host}")
       return  
    
    # Turn the commit (32-bit integer) into a hexadecimal number
    commit = commit.replace("0x","")

    # Execute the comparison
    try:
        if repo_host == "github":
            comparison = compare_github_commit_with_latest_tag(project_identifier, api_token, commit)

        elif repo_host == "gitlab":
            comparison = compare_gitlab_commit_with_latest_tag(project_identifier, api_token, commit)

        else:
            raise RuntimeError(f"Unknown host service for the repo: {repo_host}")

        latest_tag = comparison["latest_tag"]
        logger.info(f"Latest FW for project {project_identifier}: {latest_tag['tag']}, {latest_tag['commit']}")
        reghelper.write(register, comparison["status"])

    except RuntimeError as e:
        logger.error(str(e))
        reghelper.write(register, FWStatus.DB_ERROR.value)
     



def format_commit(commit: str) -> str:
    """
    Format commit to truncate to first 32-bits, put a "0x" in front.
    """
    return f"0x{commit[:8].upper()}"

# 
# Utility functions for GitHub API.
# 

def get_github_repository(gh: github.Github, repo_name: str):
    """
    Gets and returns a GitHub repository object with the given repo_name.
    """
    try:
        repo = gh.get_repo(repo_name)
    
    # Repo not found
    except github.UnknownObjectException:
        raise RuntimeError(f'Repository not found on GitHub: {repo_name}')

    # Auth didn't work
    except github.BadCredentialsException:
        raise RuntimeError(f'Cannot authenticate to GitHub API, please check your access token.')

    return repo


def retrieve_latest_tag_from_github_api(repo):
    """
    Helper function to retrieve the latest tag by passing a repo object.

    Returns:
    {
        "tag"       : <tagName> (str),
        "commit"    : <commit> (str),
    }
    """
    tags = repo.get_tags()
    # Retrieve info on latest tag and return the result
    latest_tag = tags[0]

    result = {
        "tag"    : latest_tag.name,
        "commit" : format_commit(latest_tag.commit.sha),
    }

    return result


def compare_github_commit_with_latest_tag(repo_name: str, token: str,  commit: str) -> Dict[str, int]:
    """
    For the given GitHub repository, compare the given commit with the latest tag
    using the GitHub API. Find out if the commit is more recent, older or
    identical to the tag.

    Returns a dictionary of the following form:
    {
        "status": <comparison_status> (int),
        "latest_tag": {
            "tag"    : <tagName> (str),
            "commit" : <commit> (str),
        },
    }

    where possible values of <comparison_status> are defined in the FWStatus
    enum above.
    """
    gh = github.Github(token)

    result = {}

    repo = get_github_repository(gh, repo_name)

    # First, get the latest tag
    latest_tag = retrieve_latest_tag_from_github_api(repo)

    # Add information about the latest tag to the result object
    result["latest_tag"] = latest_tag

    # Device not installed, so no FW hash is just zeros
    if all(s == '0' for s in commit):
        result["status"] = FWStatus.DEVICE_NOT_PRESENT.value
        return result
    
    # Make the comparison
    try:
        comparison = repo.compare(latest_tag["tag"], commit.replace("0x",""))

        # First, see if Git has already figured this out
        if comparison.status == "identical":
            result["status"] = FWStatus.FW_IDENTICAL.value
        elif comparison.status == "behind":
            result["status"] = FWStatus.FW_BEHIND.value
        elif comparison.status == "ahead":
            result["status"] = FWStatus.FW_AHEAD.value

        # Git might think this hash is diverged from the latest tag
        # because of merge commits. Check the base commit in this case.
        else:
            base_commit = format_commit(comparison.base_commit.sha)

            # If the base commit is the same as the tag, it means that
            # the current commit is ahead of the tag.
            if base_commit == latest_tag["commit"]:
                result["status"] = FWStatus.FW_AHEAD.value
            else:
                result["status"] = FWStatus.FW_BEHIND.value
    
    # Commit to compare cannot be found
    except github.UnknownObjectException:
        raise RuntimeError(f"Commit not found on repository: {commit}")

    return result
    
# 
# Utility functions for GitLab API.
# 

def get_gitlab_project(gl: gitlab.Gitlab, project_id: int):
    """
    Gets and returns a GitLab project object with the given project ID.
    """
    try:
        project = gl.projects.get(project_id)
    
    # Auth failed
    except gitlab.GitlabAuthenticationError:
        raise RuntimeError(f"GitLab API authentication failed, please check your access token.")
    
    return project


def retrieve_latest_tag_from_gitlab_api(project):
    """
    For the latest tag obtained with GitLab API, returns:

    {
        "tag"       : <tagName> (str),
        "commit"    : <commit> (str),
    }
    """
    try:
        latest_tag = project.tags.list()[0]
    
    except gitlab.GitlabHttpError as e:
        raise RuntimeError(f'{e.error_message} {e.response_code}')

    # Retrieve information that we want
    result = {
        "tag"    : latest_tag.name,
        "commit" : format_commit(latest_tag.commit["short_id"]),
    }

    return result


def compare_gitlab_commit_with_latest_tag(project_id: int, token: str, commit: str) -> Dict[str, str]:
    """
    For the given project ID on GitLab, compare the given commit with the latest tag obtained
    using GitLab API. Find out if the commit is more recent, older or
    identical to the tag.

    Returns a dictionary of the following form:
    {
        "status": <comparison_status> (int),
        "latest_tag": {
            "tag"    : <tagName> (str),
            "commit" : <commit> (str),
        },
    }
    
    where possible values of <comparison_status> are defined in the FWStatus
    enum above.
    """
    gl = gitlab.Gitlab(private_token=token, url="https://gitlab.com/")

    result = {}

    project = get_gitlab_project(gl, project_id)

    # First, get the latest tag
    latest_tag = retrieve_latest_tag_from_gitlab_api(project)

    result["latest_tag"] = latest_tag

    # Device not installed, so no FW hash is just zeros
    if all(s == '0' for s in commit):
        result["status"] = FWStatus.DEVICE_NOT_PRESENT.value
        return result
    
    # Make the comparison
    try:
        comparison = project.repository_compare(latest_tag["tag"], commit.replace("0x",""))

        if comparison["compare_same_ref"]:
            result["status"] = FWStatus.FW_IDENTICAL.value
        
        elif len(comparison["commits"]) == 0:
            result["status"] = FWStatus.FW_BEHIND.value
        
        else:
            result["status"] = FWStatus.FW_AHEAD.value

    # Commit to compare cannot be found
    except gitlab.GitlabGetError:
        raise RuntimeError(f"Commit not found on repository: {commit}")

    return result
