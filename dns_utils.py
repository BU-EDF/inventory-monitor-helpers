#
# DNS server related functionality.
# DNS server will be configured according to the info provided on 
# the "config.yaml" configuration file, under "dns" entry.
#

import os
import dns.resolver
import yaml

from typing import Optional


def configure_and_return_dns_server(config: dict) -> Optional[dns.resolver.Resolver]:
    """
    Given the app configuration specifying DNS server
    information, configure and return an instance of the DNS server to
    be used within the application.

    If the "dns" top-level entry is not found in the YAML file, the interpretation
    is that no DNS server will be used, and this function returns None.
    """
    # No DNS-server related information in the configuration
    if "dns" not in config:
        return None

    # Create the DNS resolver object and configure it
    dns_server = dns.resolver.Resolver()
    dns_config = config["dns"]

    # Override the DNS server with the one specified
    if dns_config.get("server"):
        dns_server.nameservers = dns_config["server"]
    
    # Override the search domain with the one(s) specified
    if dns_config.get("search_domain"):
        search_list = []
        for value in dns_config["search_domain"]:
            search_list.append(dns.name.from_text(value))
        dns_server.search = search_list
    
    return dns_server


def resolve_host_name(dns_server: dns.resolver.Resolver, hostname: str) -> str:
    """
    Given a DNS resolver object and a hostname, returns the IP address
    for that host name.

    If a host name cannot be resolved, dns.resolver.NXDOMAIN error is thrown
    by the underlying query call.
    """
    query_results = dns_server.query(hostname)

    # query_results is a list of IP address results that match the host name.
    # If multiple are returned, use the last one.
    # 
    # The "-1" indexing here is safe because if no matching IP addresses
    # are found, the "query" call above will throw an exception, which is handled
    # by the calling function.

    return str(list(query_results)[-1])
