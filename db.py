# 
# SnipeIT DB related functionality.
# 

import os
import yaml
import requests

from typing import List, Dict


def get_custom_field_from_db_item(item: dict, custom_field_name: str):
    """
    Returns the given custom field from the given database item with a type flag.
    """
    fw_hash = item["custom_fields"][custom_field_name]["value"]
    if fw_hash == '':
        return '-999'
    else:
        return fw_hash

def retrieve_tokens(token_file_path: str) -> Dict[str,str]:
    """
    Given the path to the YAML token file containing the API tokens,
    retrieve the SnipeIT, GitLab and GitHub API access keys in the 
    following format:

    {
        "snipeit" : <API key #1>,
        "gitlab"  : <API key #2>,
        "github"  : <API key #3>,
    }
    """
    # Check if the file is there
    if not os.path.exists(token_file_path):
        raise FileNotFoundError(f"Token file not found: {token_file_path}")

    # Retrieve the tokens
    with open(token_file_path, "r") as f:
        data = yaml.safe_load(f)

        if "tokens" not in data:
            raise RuntimeError(f"Invalid YAML file without 'tokens' header: {token_file_path}")        
        
        return data["tokens"]


def retrieve_response_from_snipeit_db(base_url: str, token: str) -> List:
    """
    Function that makes a GET request to SnipeIT database and retrieves
    hardware entries.

    Returns the DB response as JSON. If there is an error processing the request,
    returns an empty list.
    """
    # HTTP request headers for API auth
    HEADERS = {
        "Authorization" : f"Bearer {token}",
        "Accept"        : "application/json",
        "Content-Type"  : "application/json",
    }

    # Make the GET request and return the list of hardware
    response = requests.get(base_url, headers=HEADERS)
    
    # Handle if the response returned is an error, this can happen in two ways:
    #
    # 1. Non-200 status code is received
    # 2. 200 OK response is received, but status field is specified as "error"
    # 
    if response.status_code != 200:
        return []

    json_response = response.json()

    if "status" in json_response and json_response["status"] == "error":
        return []
    
    return json_response["rows"]


def get_apollosm_item(hw_items: list, serial: int) -> dict:
    """
    Given the list of HW items retrieved from SnipeIT DB, returns
    the item for the given Apollo Service Module. 

    Returns None if the SM with the given serial cannot be located.
    """
    result = None
    
    # Model name for the ApolloSM in the DB
    model_name = "Apollo Blade Service Module"
    
    for item in hw_items:
        if item["model"]["name"] != model_name:
            continue
        
        # Found the SM item with the correct serial
        serial_number = int(item["custom_fields"]["Apollo Serial Number"]["value"])
        if serial_number == serial:
            result = item
            break
    
    return result


def find_assigned_item_to_service_module(hw_items: list, sm_db_id: int, model_name: str) -> dict:
    """
    Find the DB entry for the assigned item to the given Service Module (sm_db_id).
    The type/model of assigned item to look for is given by the model_name parameter.

    If nothing is found, returns None.
    """
    result = None
    
    # Loop over HW items until we find the right item assigned
    for item in hw_items:
        if item["model"]["name"] != model_name:
            continue
        
        # This item is not assigned to anything
        if not item["assigned_to"]:
            continue

        # Found the item
        if item["assigned_to"]["id"] == sm_db_id:
            result = item
            break
    
    return result
